# Subscription

Dokos eases the management of recurring customer invoicing and its integration with Stripe and GoCardless for payments.

## Create a subscription

> Selling > Subscription

1. Select a customer
2. Select a subscription start date or a trial start date
3. Select the options corresponding to this subscription:
  - Generation of a sales order at the beginning of the period
  - Generation of the invoice at the beginning of the period: by default, the invoice is generated at the end of the period
  - Automatic submission of the invoice after its creation: if this option is not selected, the invoice remains a draft until manual submission
4. Add your subscription plans
5. Add taxes, charges and shipping rules for this invoice
6. If this is the case, add the discounts to applied on the next invoice
7. Don't forget your terms and conditions

### Subscription plans

The notion of subscription plan is meant to link an item to a currency, a price determination rule and a billing frequency.
You can create as many subscription plans as necessary.

You can only add subscription plans with the same currency, same price determination rule and same billing frequency.

If your subscription plan is linked to a Stripe plan, you can add it in the section **Gateways Payment Plans**

### Start and end dates

Each subscription plan can have a subscription start and end date.
During invoices generation, the plan must still be valid to be taken into account.
It is not possible to invoice subscription plan pro-rata temporis.

## Generate a payment request for the first subscription month

> Dashboard > Payment request


## Payment gateways

When the customer makes a payment for its first subscription month, the payment gateway linked to this subscription is automatically updated.
It allows the automatic generation of the following payments and a link with any webhook received from the payment gateway.

## Cancel a subscription

> Actions > Cancel Subscription

Select the cancellation date for this subscription.
If the last invoice must be generated pro-rata temporis, check the corresponding checkbox.
