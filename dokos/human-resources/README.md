# Table of content

- [Leave Type](/dokos/human-resources/leave-type.md)
- [Leave Policy](/dokos/human-resources/leave-policy.md)
- [Leave Period](/dokos/human-resources/leave-period.md)