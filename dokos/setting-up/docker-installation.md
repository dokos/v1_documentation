---
tags: ["setting up", "docker"]
---
# Docker Installation

:::warning
This installation is still experimental
:::

This page will explain how to install Dokos in a Docker container.  
This installation method is currently only available for the development branch. The master (production) branch will soon be available also.  

## Pre-requisites

In order to install Dokos on Docker, you need to install the following components on your server:
- Docker
- Docker-compose
- Git

You can find more information on how to install `docker` and `docker-compose` on the [docker website](https://docs.docker.com)


## Download Dokidocker

In order to facilitate the installation and the maintenance of the Docker images, we have created a tool called `dokidocker`.  
This tool is based on the outstanding work done by the [frappe-docker](https://github.com/frappe/frappe_docker) team.  

In order to download `dokidocker`, run the following command on your server:

```
git clone https://gitlab.com/dokos/dokidocker.git
cd dokidocker
```

## Configure your environment

Before launching the docker images, you need to define a number of environment variables.  
Start by creating an environment file by copying the provided template:

```
cp env-template .env
```

Then edit the newly created file to customize each environment variables.  
You can use any text editor or a command line editor like for example `nano .env`.  

The default variables are:
- **DOKOS_VERSION**: Defines the version to use. `latest` refers to the latest development version. For other available tags, check the [container registry](https://gitlab.com/dokos/dokidocker/container_registry/eyJuYW1lIjoiZG9rb3MvZG9raWRvY2tlci9kb2tvcy13b3JrZXIiLCJ0YWdzX3BhdGgiOiIvZG9rb3MvZG9raWRvY2tlci9yZWdpc3RyeS9yZXBvc2l0b3J5LzEyMzM2OTkvdGFncz9mb3JtYXQ9anNvbiIsImlkIjoxMjMzNjk5fQ==)
- **DODOCK_VERSION**: Defines the version to use. `latest` refers to the latest development version. For other available tags, check the [container registry](https://gitlab.com/dokos/dokidocker/container_registry/eyJuYW1lIjoiZG9rb3MvZG9raWRvY2tlci9kb2RvY2std29ya2VyIiwidGFnc19wYXRoIjoiL2Rva29zL2Rva2lkb2NrZXIvcmVnaXN0cnkvcmVwb3NpdG9yeS8xMjMyOTU3L3RhZ3M%2FZm9ybWF0PWpzb24iLCJpZCI6MTIzMjk1N30=)
- **MARIADB_HOST**: Hostname for MariaDB. Keep `mariadb` if you use the default database container.
- **MYSQL_ROOT_PASSWORD**: Password to access the MariaDB database launched as a container. If you use a managed MariaDB instance, you don't need to setup a password here.
- **SITE_NAME**: Site created after the launch of the containers. The site name must be a full domain/subdomain, e.g. `erp.example.com` or `erp.localhost`
- **SITES**: List of sites included in this deployment. If Let's Encrypt is set up, make sure that your DNS parameters are correctly pointing to the current server.
- **DB_ROOT_USER**: MariaDB root username.
- **ADMIN_PASSWORD**: Dodock/Dokos administrator password.
- **INSTALL_APPS**: App contained in the current image to be installed on the site defined in SITE_NAME.
- **ENTRYPOINT_LABEL**: Traefik configuration. For local deployment it should be `web`, for production `websecure`.
- **CERT_RESOLVER_LABEL**: Traefik resolver to use to get a TLS certificate. Use `dodock.local.no-cert-resolver` for local setup.
- **LETSENCRYPT_EMAIL**: Email address for LetsEncrypt expiry notification.
- **HTTPS_REDIRECT_RULE_LABEL**: Traefik https redirection configuration. Use `dodock.local.no-redirect-rule` for local setup.
- **HTTPS_REDIRECT_ENTRYPOINT_LABEL**: Traefik https redirection configuration. Use `dodock.local.no-entrypoint` for local setup.
- **HTTPS_REDIRECT_MIDDLEWARE_LABEL**: Traefik https redirection configuration. Use `dodock.local.no-middleware` for local setup.
- **HTTPS_USE_REDIRECT_MIDDLEWARE_LABEL**: Traefik https redirection configuration. Use `dodock.local-no-redirect-middleware` for local setup.


## Start the containers

In order to start Dokos, choose a project name and launch the following command:

`docker-compose --project-name <name-of-you-project> up -d`

## Update your site

From the `dokidocker` folder, start by editing the versions of Dodock and Dokos by modifying the values of __DODOCK_VERSION__ and __DOKOS_VERSION__.
If you are using images not pointing to a specific version, this step is not necessary.

`nano .env`

Pull the new images
`docker-compose pull`

And restart your containers
`docker-compose --project-name <name-of-you-project> up -d`

## Notes

- The first initialization can take up to a few minutes.  
You can use the command `docker logs <name-of-you-project>_site-creator_1 -f` to monitor its progress.
- After the site creation you can access it using `Administrator` as username and the password defined against variable __ADMIN_PASSWORD__.
- The site names are limited to patterns matching *.localhost by default.  
Additional site name patterns can be added by editing /etc/hosts of your host machine
