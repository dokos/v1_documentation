# Bank Reconciliation

The bank reconciliation consists in matching the operations registered on your bank statement with the transactions recorded in your ERP.

## Bank statement import

### OFX format

1. Download your bank statement in OFX format.
2. Go to **Bank Transaction**, click on _Menu_ and select **Upload an OFX statement**
3. Upload your transactions

With the OFX format, _Dokos_ will not create duplicates since each transaction is uniquely identified.
You can therefore import a file containing data already registered.

### CSV/XLSX format

1. Select the bank you wish to import a statement for in the **Bank** list.
2. In the _Data import configuration_ section, create a mapping between the header data of your CSV/XLSX file and the data from the **Bank Transaction** document in _Dokos_.

![Transactions mapping](/images/accounting/bank_reconciliation/transaction_mapping.png)

3. Download your bank statement in CSV or XLSX format. Check that the header line is the first line of the file.
4. Go to **Bank Transaction**, click on _Menu_ and select **Upload a csv/xlsx statement**
5. Upload your transactions

With the XLSX/Csv format, if the field `reference_number` is not defined, the software will not be able to uniquely identify the transactions and will import duplicates.

## Reconcile the bank statement lines

> Open the page **Bank Reconciliation**

1. Select the appropriate bank account and period
2. Select one or several bank transactions
3. Select the type of transaction to reconcile
4. Check that the amount of bank transactions matches the amount of the system transactions
5. Click on **Reconcile**

### Features

#### 1 <-> n / n <-> n reconciliation

The reconciliation tool allows a reconciliation between one or several lines of your bank statement with one transaction in _Dokos_ (Payment entry, journal entry, ...)

It also allows a reconciliation between one line on your bank statement and several transactions in _Dokos_.

But it doesn't handle the reconciliation of several lines on your bank statement with several transactions at the same time.

::: tip Astuce
If the options proposed by the tool don't handle a particular use case, you can always open the transaction and add reconciliation line by allocating the reconciled amounts manually.
:::

#### Automatic payment creation

If you reconcile an invoice, _Dokos_ will automatically create the corresponding payment.
If the invoice currency is not the invoice of the bank account, you will need to create the payment manually first.


### Rapprochement automatique

#### Stripe

If you use Stripe as payment gateway, _Dokos_ can reconcile your payments with the payouts made by Stripe on your bank account.
You only need to add Stripe "Charge" ID in the field "Cheque/Reference No" in the Payment Entry.

#### Document name

If you ask you customers to add the invoice number in the wire transfer references, _Dokos_ can make the reconciliation automatically when you click on "Automatic reconciliation".

#### Regional options

The **regional_reconciliation** method allows the configuration of reconciliation methods specific to some regions.

##### Switzerland

You can add a field **esr_reference** in your invoices in order for _Dokos_ to automatically reconcile them based on the ESR number.

#### Hook

The **auto_reconciliation_methods** hook allows the creation of your own reconciliation methods in a custom application.
