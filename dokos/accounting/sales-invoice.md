# Sales invoice

A sales invoice is a bill that you send to your customers, against which the customer processes the payment.  
A sales invoice is also an accounting transaction: On submission of a sales invoice, the system updates the receivable and books an income against a customer account.

### 1. How to create a sales invoice
1. Go to: **Accounts > Billing > Sales Invoice > New**.
2. Select the customer. 
3. Set the Payment Due Date.
4. In the items table, select the items and set the quantities.
5. The prices will be fetched automatically.
6. The posting date and time will be set to current, you can edit after you tick the checkbox below Posting Time.
7. Optionally, you can make a credit note by clicking on the corresponding button.
8. Save and submit.

To fetch the details automatically in a Sales Invoice, click on the __Get Items from__.  
The details can be fetched from a Sales Order, Delivery Note, or a Quotation. 


### 2. Features
#### 2.1 Dates

__Posting Date__: The date on which the Sales Invoice will affect your books of accounts i.e. your General Ledger.  
This will affect all your balances in that accounting period.

__Due Date__: The date on which the payment is due (if you have sold on credit).  
If the due date is not passed, the invoice will have status "Unpaid".  
After the due date, it will be considered as "Overdue".  

#### 2.2 Automatically Fetching Item Batch Numbers

If you are selling an item from a batch, dokos will automatically fetch a batch number for you if "Update Stock" is checked.  
The batch number will be fetched on a First Expiring First Out (FEFO) basis.  
This is a variant of First In First Out (FIFO) that gives highest priority to the soonest to expire Items. 


Note that if the first batch in the queue cannot satisfy the order on the invoice, the next batch in the queue that can satisfy the order will be selected.  
If there is no batch that can satisfy the order, dokos will cancel its attempt to automatically fetch a suitable batch number.

#### 2.3 POS Invoices

In a scenario where you use the point of sales, the field __Is POS__ is automatically checked upon payment of the invoice.  
Your **POS Profile** data are fetched into the sales invoice and the payment is registered directly into the sales invoice.  
If the stock needs to be updated, the checkbox __Update Stock__ will be automatically checked.  


#### 2.4 Billing Timesheet with Project

If you want to bill employees working on Projects on hourly basis, they can fill out Timesheets which consists their billing rate.  
When you make a new Sales Invoice, select the Project for which the billing is to be made, and the corresponding Timesheet entries for that Project will be fetched.
Add the item you wish to invoice and report the billable amount in the corresponding item line.   


#### 2.5 "Pro Forma" Invoice

If you want to give an Invoice to a Customer to make a payment before you deliver, i.e. you operate on a payment first basis, you should create a
Quotation or a Sales Order and title it as a “Pro-forma Invoice” using the Print Heading feature.

### 3. More
#### Accounting Impact

All Sales must be booked against an income account (+ VAT account if applicable) and a customer account
The income account is determined based on the selected item, it is therefore very important to setup your items by affecting them the right income account before making a transaction.

:::tip
You can also define the income account at the item group or company level if you use only one account of all your transactions.
:::

The other account is the customer account, which is defined in the customer master data or in the company master data if you use the same account for all your customers.
In dokos, you don't need to create an account for each customer. The system registers all transactions against auxiliary accounts corresponding to each customer.  

You can also register your transaction by linking them to a cost center.  
It has no accounting impact, but it allows you to generate a profit and loss statementn a balance sheet and to calculate the profitability of each of your lines of business.

#### Accounting entries (GL Entry) for a typical double entry “Sale”:

|**Debit**|**Credit:**|
|---------|-----------|
| - Customer (Grand total) | - Income (net total for each Item) <br>- Taxes|

> To see entries in your Sales Invoice after you “Submit”, click on “View Ledger”.