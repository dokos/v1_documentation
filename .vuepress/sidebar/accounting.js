module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				children: [
					'',
					'journaux-comptables',
					'facture-vente',
					'ecriture-de-paiement',
					'demande-paiement',
					'prelevement-sepa',
					'passerelles-paiement',
					'rapprochement-bancaire'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			children: [
				'',
				'accounting-journal',
				'sales-invoice',
				'payment-entry',
				'payment-request',
				'sepa-direct-debit',
				'payment-gateways',
				'bank-reconciliation'
			]
		}
	]
}