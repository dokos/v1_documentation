# Fournisseur

Un fournisseur est une personne physique ou morale auprès de qui vous achetez des produits ou des services.

## 1. Créer un fournisseur

> Achats > Fournisseur

Un fournisseur est une donnée de base, ne nécessitant aucun pré-requis.

### Paramétrages par défaut

Afin de faciliter la création de vos fournisseur, il est possible de définir certaines informations par défaut dans les **paramètres d'achat**.

1. **Système de nommage**: Chaque fournisseur possède un identifiant unique, qui est le nom du document dans lequel sont renseignés les informations du fournisseur. Cet identifiant du fournisseur peut être basé sur le nom complet du fournisseur ou sur un numéro de série.  
Si vous choisissez de le baser sur le nom complet du fournisseur, en cas d'homonymes, _Dokos_ rajoutera automatiquement un "-1" à la fin du nom pour les différencier.  
Si vous choisissez de le baser sur une série, vous pouvez définir le préfixe de cette série dans le type de document **Nom de série**. 

2. **Groupe de fournisseurs par défaut**: Définissez le groupe de fournisseurs par défaut. Il s'agit généralement du groupe de fournisseurs le plus souvent sélectionné.

3. **Liste de prix d'achat par défaut**: Définissez la liste de prix qui sera sélectionnée par défaut dans lorsque vous créez un nouveau fournisseur.

### Fonctionnalités

Les informations renseignées dans la fiche fournisseur servent ensuite de valeurs par défaut dans les transactions.
Par exemple, si vous mettez "USD" comme devise de facturation dans la fiche du fournisseur, les documents générés pour ce fournisseur (commande, factures, ...) seront automatiquement en US Dollars. Vous pouvez toujours modifier cette valeur dans le document correspondant au moment de sa création.