# Intégration Google

__dokos__ peut être connecté avec plusieurs outils de la suite d'applications Google: Google Agenda, Google Drive, Google Contacts et Google Maps

# 1. Paramètres Google

> Intégrations > Paramètres Google

Afin de connecter les outils Google avec __dokos__, vous devez activer et paramétrer une intégration Oauth avec la plateforme Google Cloud.
Cela permet d'obtenir un jeton d'accès (autorisation d'accéder au service) de la part de Google pour le service que vous souhaitez utiliser.

### 1.1. Paramétrage pour Agenda, Drive and Contacts

1. Allez sur [https://console.cloud.google.com/](https://console.cloud.google.com/)
1. Utilisez un projet existant ou créez un nouveau projet
1. Dans **API and Services** sélectionnez **Credentials**
1. Cliquez sur **Create credentials** et sélectionnez **OAuth Client ID**
![OAuth Client Creation](/images/setup/google_setup/oauth_client_creation.png)
1. Sélectionnez **Web Application**
1. Dans **Authorized Javascript origins** ajoutez l'URL de votre site: `https://{votresite}` (Ex. __https://dokos.io__)
1. Dans **Authorized redirect URIs** ajoutez les URLs suivantes en fonction de vos besoins d'intégration:
    - Google Agenda: `https://{votresite}?cmd=frappe.integrations.doctype.google_calendar.google_calendar.google_callback`
    - Google Contacts: `https://{votresite}?cmd=frappe.integrations.doctype.google_contacts.google_contacts.google_callback`
    - Google Drive: `https://{votresite}?cmd=frappe.integrations.doctype.google_drive.google_drive.google_callback`
1. Enregistrez et copiez/collez le **Client ID** et **Client Secret** générés dans les champs correspondants du document **Google Settings** de __dokos__
1. Allez dans **OAuth consent screen** et ajoutez votre domaine aux **Authorized domains**
![OAuth Consent Setup](/images/setup/google_setup/oauth_consent_setup.png)
1. Allez dans **Library** et activez les APIs nécessaires: **Contacts API**, **Google Calendar API** et/ou **Google Drive API**

### 1.2. Paramétrage pour Maps

L'accès à Google Maps ne nécessite pas de connexion OAuth, seulement une clé API spécifique.

1. Allez sur [https://console.cloud.google.com/](https://console.cloud.google.com/)
1. Utilisez un projet existant ou créez un nouveau projet
1. Dans **API and Services** sélectionnez **Credentials**
1. Cliquez sur **Create credentials** et sélectionnez **API key**
1. Allez dans **Bibliothèque** et activez la librairie **Geocoding API**
1. Copiez et collez cette clé dans le champs **API key** du document **Google Settings** de __dokos__

# 2. Google Agenda

> Intégrations > Google Agenda

Vous pouvez créer autant de Calendrier Google que vous le souhaitez.
Chaque calendrier est lié à un seul document de référence et, optionnellement, à un utilisateur.

Actuellement, l'intégration Google Agenda fonctionne avec les types de documents suivants:
- Evénement
- Réservation d'articles

### 2.1. Créer un nouveau calendrier

1. Donnez un nom reconnaissable à votre calendrier
1. Sélectionnez un document de référence: Ce calendrier sera seulement sélectionnable dans le document de référence.
1. Ajouter un utilisateur si l'option est proposée. Si un utilisateur est défini, le calendrier sera accessible seulement à cet utilisateur.
1. Vérifiez vos options de synchronisation.
1. Vous pouvez ensuite **Autoriser l'accès à Google Agenda**
1. Une fois l'accès accordé, vous pouvez ajouter l'identifiant Google Agenda de l'un de vos calendriers existants ou laisser ce champ vide.
   Si le champ est vide, un nouveau calendrier sera automatiquement créé.